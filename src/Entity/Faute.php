<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FauteRepository")
 */
class Faute
{

    public CONST CARTONS = [
        1 => "Rouge",
        2 => "Jaune"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $temps;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Joueur", inversedBy="fautes")
     */
    private $joueur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Arbitre")
     * @ORM\JoinColumn(nullable=false)
     */
    private $arbitre;

    /**
     * @ORM\ManyToOne(targetEntity="Rencontre", inversedBy="fautes")
     */
    private $rencontre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sanction")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sanction;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Carton")
     */
    private $carton;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTemps(): ?int
    {
        return $this->temps;
    }

    public function setTemps(int $temps): self
    {
        $this->temps = $temps;

        return $this;
    }

    /**
     * @return Joueur
     */
    public function getJoueur(): Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(?Joueur $joueur) {
        $this->joueur = $joueur;

        return $this;
    }

    public function getArbitre(): ?Arbitre
    {
        return $this->arbitre;
    }

    public function setArbitre(?Arbitre $arbitre): self
    {
        $this->arbitre = $arbitre;

        return $this;
    }

    /**
     * @return Rencontre
     */
    public function getRencontre(): Rencontre
    {
        return $this->rencontre;
    }

    public function setRencontre(?Rencontre $rencontre): self
    {
        $this->rencontre = $rencontre;

        return $this;
    }

    public function getSanction(): ?Sanction
    {
        return $this->sanction;
    }

    public function setSanction(?Sanction $sanction): self
    {
        $this->sanction = $sanction;

        return $this;
    }

    public function getCarton(): ?Carton
    {
        return $this->carton;
    }

    public function setCarton(?Carton $carton): self
    {
        $this->carton = $carton;

        return $this;
    }

    public function __toString() {
        return "faute";
    }
}
