<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RencontreRepository")
 */
class Rencontre
{
    const MAILLOTS = [
        "Principal" => 1,
        "Secondaire" => 2
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $equipeAdverse;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lieu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="boolean")
     */
    private $typeMaillot;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $finMatch;

    /**
     * @ORM\ManyToMany(targetEntity="Arbitre")
     */
    private $arbitres;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $premiereMiTemps;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $secondeMiTemps;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Faute", mappedBy="rencontre")
     */
    private $fautes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ButAdverse", mappedBy="rencontre")
     */
    private $butAdverses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\But", mappedBy="rencontre")
     */
    private $but;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ArbitreRencontre", mappedBy="rencontre")
     */
    private $arbitreMatches;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\JoueurRencontre", mappedBy="rencontre")
     */
    private $joueurMatches;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Changement", mappedBy="rencontre")
     */
    private $changements;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\JoueurRencontre")
     */
    private $capitaine;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\JoueurRencontre")
     */
    private $suppleant;

    public function __construct()
    {
        $this->arbitres = new ArrayCollection();
        $this->fautes = new ArrayCollection();
        $this->butAdverses = new ArrayCollection();
        $this->arbitreMatches = new ArrayCollection();
        $this->joueurMatches = new ArrayCollection();
        $this->changements = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEquipeAdverse(): ?string
    {
        return $this->equipeAdverse;
    }

    public function setEquipeAdverse(string $equipeAdverse): self
    {
        $this->equipeAdverse = $equipeAdverse;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTypeMaillot(): ?int
    {
        return $this->typeMaillot;
    }

    public function setTypeMaillot(int $typeMaillot): self
    {
        $this->typeMaillot = $typeMaillot;

        return $this;
    }

    /**
     * @return Collection|Arbitre[]
     */
    public function getArbitres(): Collection
    {
        return $this->arbitres;
    }

    public function addArbitre(Arbitre $referee): self
    {
        if (!$this->arbitres->contains($referee)) {
            $this->arbitres[] = $referee;
        }

        return $this;
    }

    public function removeArbitre(Arbitre $arbitre): self
    {
        if ($this->arbitres->contains($arbitre)) {
            $this->arbitres->removeElement($arbitre);
        }

        return $this;
    }

    public function getPremiereMiTemps(): ?int
    {
        return $this->premiereMiTemps;
    }

    public function setPremiereMiTemps(?int $premiereMiTemps): self
    {
        $this->premiereMiTemps = $premiereMiTemps;

        return $this;
    }

    public function getSecondeMiTemps(): ?int
    {
        return $this->secondeMiTemps;
    }

    public function setSecondeMiTemps(?int $secondeMiTemps): self
    {
        $this->secondeMiTemps = $secondeMiTemps;

        return $this;
    }

    /**
     * @return Collection|Faute[]
     */
    public function getFautes(): Collection
    {
        return $this->fautes;
    }

    public function addFaute(Faute $faute): self
    {
        if (!$this->fautes->contains($faute)) {
            $this->fautes[] = $faute;
            $faute->setRencontre($this);
        }

        return $this;
    }

    public function removeFaute(Faute $faute): self
    {
        if ($this->fautes->contains($faute)) {
            $this->fautes->removeElement($faute);
            $faute->setRencontre(null);
        }

        return $this;
    }

    /**
     * @return Collection|ButAdverse[]
     */
    public function getButAdverses(): Collection
    {
        return $this->butAdverses;
    }

    public function addButAdverse(ButAdverse $butAdverse): self
    {
        if (!$this->butAdverses->contains($butAdverse)) {
            $this->butAdverses[] = $butAdverse;
            $butAdverse->setRencontre($this);
        }

        return $this;
    }

    public function removeButAdverse(ButAdverse $butAdverse): self
    {
        if ($this->butAdverses->contains($butAdverse)) {
            $this->butAdverses->removeElement($butAdverse);
            // set the owning side to null (unless already changed)
            if ($butAdverse->getRencontre() === $this) {
                $butAdverse->setRencontre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|But[]
     */
    public function getBut(): Collection
    {
        return $this->but;
    }

    public function addBut(But $but): self
    {
        if (!$this->but->contains($but)) {
            $this->but[] = $but;
            $but->setRencontre($this);
        }

        return $this;
    }

    public function removeBut(But $but): self
    {
        if ($this->but->contains($but)) {
            $this->but->removeElement($but);
            // set the owning side to null (unless already changed)
            if ($but->getRencontre() === $this) {
                $but->setRencontre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArbitreRencontre[]
     */
    public function getArbitreMatches(): Collection
    {
        return $this->arbitreMatches;
    }

    public function addArbitreMatch(ArbitreRencontre $arbitreMatch): self
    {
        if (!$this->arbitreMatches->contains($arbitreMatch)) {
            $this->arbitreMatches[] = $arbitreMatch;
            $arbitreMatch->setRencontre($this);
        }

        return $this;
    }

    public function removeArbitreMatch(ArbitreRencontre $arbitreMatch): self
    {
        if ($this->arbitreMatches->contains($arbitreMatch)) {
            $this->arbitreMatches->removeElement($arbitreMatch);
            // set the owning side to null (unless already changed)
            if ($arbitreMatch->getRencontre() === $this) {
                $arbitreMatch->setRencontre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|JoueurRencontre[]
     */
    public function getJoueurMatches(): Collection
    {
        return $this->joueurMatches;
    }

    public function addJoueurMatch(JoueurRencontre $joueurMatch): self
    {
        if (!$this->joueurMatches->contains($joueurMatch)) {
            $this->joueurMatches[] = $joueurMatch;
            $joueurMatch->setRencontre($this);
        }

        return $this;
    }

    public function removeJoueurMatch(JoueurRencontre $joueurMatch): self
    {
        if ($this->joueurMatches->contains($joueurMatch)) {
            $this->joueurMatches->removeElement($joueurMatch);
            // set the owning side to null (unless already changed)
            if ($joueurMatch->getRencontre() === $this) {
                $joueurMatch->setRencontre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Changement[]
     */
    public function getChangements(): Collection
    {
        return $this->changements;
    }

    public function addChangement(Changement $changement): self
    {
        if (!$this->changements->contains($changement)) {
            $this->changements[] = $changement;
            $changement->setRencontre($this);
        }

        return $this;
    }

    public function removeChangement(Changement $changement): self
    {
        if ($this->changements->contains($changement)) {
            $this->changements->removeElement($changement);
            // set the owning side to null (unless already changed)
            if ($changement->getRencontre() === $this) {
                $changement->setRencontre(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getFinMatch()
    {
        return $this->finMatch;
    }

    /**
     * @param int $finMatch
     */
    public function setFinMatch($finMatch): void
    {
        $this->finMatch = $finMatch;
    }

    public function getCapitaine(): ?JoueurRencontre
    {
        return $this->capitaine;
    }

    public function setCapitaine(?JoueurRencontre $capitaine): self
    {
        $this->capitaine = $capitaine;

        return $this;
    }

    public function getSuppleant(): ?JoueurRencontre
    {
        return $this->suppleant;
    }

    public function setSuppleant(?JoueurRencontre $suppleant): self
    {
        $this->suppleant = $suppleant;

        return $this;
    }
}
