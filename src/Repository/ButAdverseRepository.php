<?php

namespace App\Repository;

use App\Entity\ButAdverse;
use App\Entity\Rencontre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ButAdverse|null find($id, $lockMode = null, $lockVersion = null)
 * @method ButAdverse|null findOneBy(array $criteria, array $orderBy = null)
 * @method ButAdverse[]    findAll()
 * @method ButAdverse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ButAdverseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ButAdverse::class);
    }

//     /**
//      * @return ButAdverse[] Returns an array of ButAdverse objects
//      */
//    public function findSumByMatch()
//    {
//        return $this->createQueryBuilder('b')
//            ->select('COUNT(b), b.rencontre')
//            ->groupBy('b.rencontre')
////            ->orderBy('b.rencontre', 'ASC')
//            ->getQuery()
////            ->getResult()
//            ->getScalarResult()
//        ;
//    }

    public function findByMatchId($idMatch)
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b)')
            ->andWhere('b.rencontre = :idMatch')
            ->setParameter('idMatch', $idMatch)
            ->groupBy('b.rencontre')
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?ButAdverse
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
