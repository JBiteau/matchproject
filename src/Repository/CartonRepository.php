<?php

namespace App\Repository;

use App\Entity\Carton;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Carton|null find($id, $lockMode = null, $lockVersion = null)
 * @method Carton|null findOneBy(array $criteria, array $orderBy = null)
 * @method Carton[]    findAll()
 * @method Carton[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartonRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Carton::class);
    }

    // /**
    //  * @return Carton[] Returns an array of Carton objects
    //  */



    /*
    public function findOneBySomeField($value): ?Carton
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
