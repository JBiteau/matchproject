<?php

namespace App\Repository;

use App\Entity\Position;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Position|null find($id, $lockMode = null, $lockVersion = null)
 * @method Position|null findOneBy(array $criteria, array $orderBy = null)
 * @method Position[]    findAll()
 * @method Position[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PositionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Position::class);
    }

    // /**
    //  * @return Position[] Returns an array of Position objects
    //  */

    public function findOrderedList()
    {
        $results = $this->createQueryBuilder('p')
            ->select('p.id, p.nom')
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getScalarResult()
        ;

        $returnList = [];

        foreach ($results as $result){
            $returnList[$result['id']] = $result['nom'];
        }

        return $returnList;
    }


    /*
    public function findOneBySomeField($value): ?Position
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
