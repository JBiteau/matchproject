<?php

namespace App\Form;

use App\Entity\ArbitreRencontre;
use App\Entity\Carton;
use App\Entity\Faute;
use App\Entity\JoueurRencontre;
use App\Entity\Sanction;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FauteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('joueur', EntityType::class, [
                'mapped' => false,
                'label' => "Joueur",
                'class' => JoueurRencontre::class,
                'choices' => $options['joueurs'],
            ])
            ->add('arbitre', EntityType::class, [
                'mapped' => false,
                'label' => "Arbitre",
                'class' => ArbitreRencontre::class,
                'choices' => $options['arbitres'],
            ])
            ->add('sanction', EntityType::class, [
                'mapped' => false,
                'label' => "Sanction",
                'class' => Sanction::class,
            ])
            ->add('carton', EntityType::class, [
                'mapped' => false,
                'label' => "Carton",
                'class' => Carton::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Faute::class,
            'joueurs' => null,
            'arbitres' => null
        ]);
    }
}
