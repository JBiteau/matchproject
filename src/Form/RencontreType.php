<?php

namespace App\Form;

use App\Entity\Rencontre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RencontreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        try {
            $builder
                ->add( 'equipeAdverse', null, [
                    'label' => "Equipe adverse"
                ] )
                ->add( 'lieu', null, [
                    'label' => "Lieu"
                ] )
                ->add( 'date', null, [
                    'label' => "Date",
                    'data' => new \DateTime( "now" )
                ] )
                ->add( 'typeMaillot', ChoiceType::class, [
                    'choices' => Rencontre::MAILLOTS,
                    'label' => "Type de maillot pour cette rencontre"
                ] );
        } catch (\Exception $e) {
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rencontre::class,
        ]);
    }
}
