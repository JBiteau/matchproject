<?php

namespace App\Form;

use App\Entity\Changement;
use App\Entity\JoueurRencontre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        dump($options);
        $builder
            ->add('tempsDeSortie')
            ->add('entreeJoueur', EntityType::class, [
                'data' => 0,
                'required' => false,
                'mapped' => false,
                'label' => "Joueur qui entre",
                'class' => JoueurRencontre::class,
                'choices' => $options['joueursRemplacants'],
            ])
            ->add('sortieJoueur', EntityType::class, [
                'mapped' => false,
                'label' => "Joueur qui sort",
                'class' => JoueurRencontre::class,
                'choices' => $options['joueurs'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Changement::class,
            'joueurs' => null,
            'joueursRemplacants' => null
        ]);
    }
}
