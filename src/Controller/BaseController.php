<?php

namespace App\Controller;

use App\Entity\Rencontre;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(EntityManagerInterface $entityManager)
    {
        $nextMatchs = $entityManager->getRepository(Rencontre::class)->findNext();

        return $this->render('index.html.twig', [
            'matchs' => $nextMatchs,
        ]);
    }
}
