<?php

namespace App\Controller;

use App\Entity\Arbitre;
use App\Entity\ArbitreRencontre;
use App\Entity\But;
use App\Entity\ButAdverse;
use App\Entity\Carton;
use App\Entity\Changement;
use App\Entity\Faute;
use App\Entity\JoueurRencontre;
use App\Entity\Joueur;
use App\Entity\Position;
use App\Entity\Poste;
use App\Entity\Rencontre;
use App\Entity\Sanction;
use App\Form\ArbitreRencontreType;
use App\Form\ButAdverseType;
use App\Form\ButType;
use App\Form\ChangementType;
use App\Form\FauteType;
use App\Form\JoueurMatchType;
use App\Form\RencontreType;
use App\Repository\ButAdverseRepository;
use App\Repository\ButRepository;
use App\Repository\JoueurMatchRepository;
use App\Repository\RencontreRepository;
use App\Utils\RencontreUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rencontre", name="match")
 */
class RencontreController extends AbstractController
{
    /**
     * @Route("/", name="_index")
     * @param RencontreRepository $rencontreRepository
     * @param JoueurMatchRepository $joueurMatchRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(RencontreRepository $rencontreRepository, ButAdverseRepository $butAdverseRepository, ButRepository $butRepository)
    {
        $allScoreRencontres = $rencontreRepository->getAllScore();

        $matchs = [];
        $butsParMatch = [];
        $butAdverseParMatch = [];

        foreach ($allScoreRencontres as $scoreRencontre){
            array_push($matchs,$rencontreRepository->find($scoreRencontre['id']));
            array_push($butsParMatch,$scoreRencontre['score_equipe']);
            array_push($butAdverseParMatch,$scoreRencontre['score_adverse']);
        }


        return $this->render('rencontre/index.html.twig', [
            'matchs' => $matchs,
            'buts' => $butsParMatch,
            'butsAdverse' => $butAdverseParMatch,
        ]);
    }

    /**
     * @Route("/add", name="_add")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {
        $match = new Rencontre();

        $form = $this->createForm(RencontreType::class, $match);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($match);
            $entityManager->flush();

            $this->addFlash("success", "Le rencontre a bien été programmé"); // TODO : displayFlash JS

            return $this->redirectToRoute("match_add_step", [
                'id' => $match->getId()
            ]);
        }

        return $this->render('rencontre/edit.html.twig', [
            'form' => $form->createView(),
            'title' => 'Programmation d\'une rencontre',
        ]);
    }

    /**
     * @Route("/add-step2/{id}", name="_add_step")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addStep2($id, Request $request, EntityManagerInterface $entityManager)
    {
        $match = $entityManager->getRepository(Rencontre::class)->find($id);

        if(!$match) {
            return $this->createNotFoundException("Le rencontre n'a pas été trouvé");
        }

        $joueurMatch = new JoueurRencontre();

        $players = [];

        foreach($match->getJoueurMatches() as $player) {
            $status = 0;

            if($match->getCapitaine() == $player) {
                $status = 1;
            } else if($match->getSuppleant() == $player) {
                $status = 2;
            }

            $players[] = [
                'id' => $player->getJoueur()->getId(),
                'name' => $player->getJoueur()->getPrenom() . " " . $player->getJoueur()->getNom(),
                'position' => ($player->getPosition() ? $player->getPosition()->getId() : 0),
                'numero' => $player->getNumero(),
                'poste' => ($player->getPoste() ? $player->getPoste()->getId() : 0),
                'estRemplacant' => $player->getEstRemplacant(),
                'status' => $status,
            ];
        }

        $arbitres = [];

        foreach ($match->getArbitreMatches() as $arbitre) {
            $arbitres[] = [
                'nom' => $arbitre->getArbitre()->getPrenom() . " " . $arbitre->getArbitre()->getNom(),
                'estPrincipal' => $arbitre->getEstPrincipal(),
            ];
        }

        $form = $this->createForm(JoueurMatchType::class, $joueurMatch);
        $arbitreForm = $this->createForm(ArbitreRencontreType::class);

        return $this->render('rencontre/edit-step2.html.twig', [
            'form' => $form->createView(),
            'form2' => $form->createView(),
            'formArbitre' => $arbitreForm->createView(),
            'match' => $match,
            'players' => $players,
            'arbitres' => $arbitres
        ]);
    }

    /**
     * @Route("/view/{id}", name="_view")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function view($id, Request $request, EntityManagerInterface $entityManager)
    {
        $match = $entityManager->getRepository(Rencontre::class)->find($id);

        if(!$match) {
            return $this->createNotFoundException("Le rencontre n'a pas été trouvé");
        }

        $players = [];

        foreach($match->getJoueurMatches() as $player) {
            $status = 0;

            if($match->getCapitaine() == $player) {
                $status = 1;
            } else if($match->getSuppleant() == $player) {
                $status = 2;
            }

            $players[] = [
                'id' => $player->getJoueur()->getId(),
                'name' => $player->getJoueur()->getPrenom() . " " . $player->getJoueur()->getNom(),
                'position' => ($player->getPosition() ? $player->getPosition()->getId() : 0),
                'numero' => $player->getNumero(),
                'poste' => ($player->getPoste() ? $player->getPoste()->getId() : 0),
                'estRemplacant' => $player->getEstRemplacant(),
                'status' => $status,
            ];
        }

        $but = new But();

        $butForm = $this->createForm(ButType::class, $but, [
            'joueurs' => $match->getJoueurMatches()->getValues()
        ]);

        $faute = new Faute();

        $fauteForm = $this->createForm(FauteType::class, $faute, [
            'joueurs' => $match->getJoueurMatches()->getValues(),
            'arbitres' => $match->getArbitreMatches()->getValues()
        ]);

        $changement = new Changement();

        $changementForm = $this->createForm(ChangementType::class, $changement, [
            'joueursRemplacants' => array_filter($match->getJoueurMatches()->getValues(), function($joueurMatch) {
                dump($joueurMatch);
                if($joueurMatch->getEstRemplacant()) {
                    return true;
                } else return false;
            }
            ),
            'joueurs' => $match->getJoueurMatches()->getValues()
        ]);

        return $this->render('rencontre/view.html.twig', [
            'butForm' => $butForm->createView(),
            'changementForm' => $changementForm->createView(),
            'fauteForm' => $fauteForm->createView(),
            'match' => $match,
            'players' => $players
        ]);
    }

    /**
     * @Route("/add-player", name="_add_player")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addPlayer(Request $request, EntityManagerInterface $entityManager) {
        if($request->isXmlHttpRequest()) {

            $id = $request->get('id');
            $playerId = $request->get('player_id');
            $posteInt = $request->get('poste');
            $numero = $request->get('numero');
            $positionInt = $request->get('position');
            $estRemplacant = $request->get('estRemplacant');
            $status = $request->get('status');

            dump($status);

            $jm = new JoueurRencontre();
            $form = $this->createForm(JoueurMatchType::class, $jm);
            $form->handleRequest($request);

            $match = $entityManager->getRepository(Rencontre::class)->find($id);
            $player = $entityManager->getRepository(Joueur::class)->find($playerId);
            $position = $entityManager->getRepository(Position::class)->find($positionInt);
            $poste = $entityManager->getRepository(Poste::class)->find($posteInt);
            dump($id);
            dump($match);
            dump($numero);

            if($match && $numero != "") {
                dump("ok la ?");
                $joueurMatch = $entityManager->getRepository(JoueurRencontre::class)->findOneBy([
                    'rencontre' => $match,
                    'joueur' => $player
                ]);

                if($joueurMatch) {
                    $jm = $joueurMatch;
                }

                $jm->setRencontre($match)
                    ->setEstRemplacant(($estRemplacant) == "true" ? true : false)
                    ->setJoueur($player)
                    ->setNumero(intval($numero))
                    ->setPosition($position)
                    ->setPoste($poste);
                $match->addJoueurMatch($jm);

                if($status == 1) {
                    $match->setCapitaine($jm);
                } else if($status == 2) {
                    $match->setSuppleant($jm);
                }

                if(!$joueurMatch) {
                    $entityManager->persist($jm);
                }

                $entityManager->flush();

                return $this->json([
                   'success' => "true"
                ]);
            }
        }

        return $this->json([
            'success' => "false"
        ]);
    }

    /**
     * @Route("/ajax/show", name="_show-player")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    function showPlayer(Request $request, EntityManagerInterface $entityManager) {
        if($request->isXmlHttpRequest()) {
            $id = $request->get("id");
            $match = $request->get('match');
            dump($id);
            dump($match);
            /** @var JoueurRencontre $player */
            $player = $entityManager->getRepository(JoueurRencontre::class)->findOneBy([
                'rencontre' => $match,
                'joueur' => $id,
            ]);
            dump($player);

            if($player) {
                $status = 0;

                $rencontre = $player->getRencontre();

                if($rencontre->getCapitaine() == $player) {
                    $status = 1;
                } else if($rencontre->getSuppleant() == $player) {
                    $status = 2;
                }

                $data = [
                    'success' => true,
                    'id' => $player->getJoueur()->getId(),
                    'position' => ($player->getPosition() ? $player->getPosition()->getId() : 0),
                    'numero' => $player->getNumero(),
                    'poste' => ($player->getPoste() ? $player->getPoste()->getId() : 0),
                    'estRemplacant' => $player->getEstRemplacant(),
                    'status' => $status,
                ];
                dump($data);
                return $this->json($data);
            }
        }

        return $this->json([
            'success' => false,
        ]);
    }

    /**
     * @Route("/ajax/remove", name="_remove-player")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    function removePlayer(Request $request, EntityManagerInterface $entityManager)
    {
        if ($request->isXmlHttpRequest()) {
            $id = $request->get("id");
            $match = $request->get('match');

            $player = $entityManager->getRepository(JoueurRencontre::class)->findOneBy([
                'rencontre' => $match,
                'joueur' => $id,
            ]);
            if($player) {
                $rencontre = $player->getRencontre();
                if($rencontre->getCapitaine() == $player) {
                    $rencontre->setCapitaine(null);
                } else if($rencontre->getSuppleant() == $player) {
                    $rencontre->setSuppleant(null);
                }
                $player->getRencontre()->removeJoueurMatch($player);

                $entityManager->remove($player);
                $entityManager->flush();

                return $this->json([
                    'success' => true
                ]);
            }
        }
        return $this->json([
            'success' => false
        ]);
    }

    /**
     * @Route("/ajax/timer", name="_update-timer")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    function updateTimer(Request $request, EntityManagerInterface $entityManager)
    {
        if ($request->isXmlHttpRequest()) {
            $id = $request->get('id');
            $step = $request->get('step');

            $rencontre = $entityManager->getRepository(Rencontre::class)->find($id);

            $now = new \DateTime();

            $now->add(new \DateInterval('PT2H'));

            $diff = $now->diff($rencontre->getDate());

            $minuteDiff = ($diff->h * 60) + $diff->i;

           /* dump($now);
            dump($rencontre->getDate());
            dump($diff);
            dump($minuteDiff);*/

            if($step == 1) {
                $rencontre->setPremiereMiTemps($minuteDiff);
            } else if($step == 2){
                $rencontre->setSecondeMiTemps($minuteDiff);
            } else if($step == 3) {
                $rencontre->setFinMatch($minuteDiff);
            }

            $entityManager->flush();

            return $this->json([
                'success' => true
            ]);
        }
    }

    /**
     * @Route("/addchangement", name="_add-changement")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    function addChangement(Request $request, EntityManagerInterface $entityManager)
    {
        if ($request->isXmlHttpRequest()) {
            $matchId = $request->get('match_id');
            $entry = $request->get('entry');
            $leave = $request->get('leave');
            $time = $request->get('timeLeave');

            $rencontre = $entityManager->getRepository(Rencontre::class)->find($matchId);
            $leavePlayer = $entityManager->getRepository(JoueurRencontre::class)->findOneBy([
                'id' => $leave,
                'rencontre' => $rencontre
            ]);

            if($rencontre && $leavePlayer && !is_null($rencontre->getPremiereMiTemps()) && is_null($rencontre->getFinMatch())) {
                $rencontreUtils = new RencontreUtils();

                $changement = new Changement();
                $changement->setRencontre($rencontre)
                    ->setTemps(($time != "" ? $time : $rencontreUtils->getTimeSinceBeginning($rencontre)))
                    ->setSortieJoueur($leavePlayer->getJoueur());

                if($entry != "") { // entrée et sortie ou sortie seule (temps ?)
                    $entryPlayer = $entityManager->getRepository(JoueurRencontre::class)->findOneBy([
                        'id' => $entry,
                        'rencontre' => $rencontre
                    ]);
/*
                    dump($entryPlayer);*/

                    if($entryPlayer) {
                        $changement->setEntreeJoueur($entryPlayer->getJoueur());

                        $rencontre->addChangement($changement);

                        $entityManager->persist($changement);
                        $entityManager->flush();

                        return $this->json([
                            'success' => true
                        ]);
                    }
                } else {
                    if($time != "") {
                        $timeSinceBeginning = $rencontreUtils->getTimeSinceBeginning($rencontre);
                        $changement->setTempsDeSortie($timeSinceBeginning);
                    }
                    $rencontre->addChangement($changement);

                    $entityManager->persist($changement);
                    $entityManager->flush();
                    return $this->json([
                        'success' => true
                    ]);
                }
            }
        }

        return $this->json([
            'success' => false
        ]);
    }

    /**
     * @Route("/addownbut", name="_add-own-but")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    function addOwnBut(Request $request, EntityManagerInterface $entityManager)
    {
        if ($request->isXmlHttpRequest()) {
            $matchId = $request->get('match_id');
            $playerId = $request->get('player');

            $rencontre = $entityManager->getRepository(Rencontre::class)->find($matchId);

            if($rencontre && !is_null($rencontre->getPremiereMiTemps()) && is_null($rencontre->getFinMatch())) {
                $player = $entityManager->getRepository(JoueurRencontre::class)->findOneBy([
                    'id' => $playerId,
                    'rencontre' => $rencontre
                ]);

                if($player) {
                    $rencontreUtils = new RencontreUtils();

                    $ownBut = new But();
                    $ownBut->setTemps($rencontreUtils->getTimeSinceBeginning($rencontre))
                        ->setRencontre($rencontre)
                        ->setJoueur($player->getJoueur());

                    $rencontre->addBut($ownBut);

                    $entityManager->persist($ownBut);
                    $entityManager->flush();

                    return $this->json([
                        'success' => true
                    ]);
                }
            }
        }

        return $this->json([
            'success' => false
        ]);
    }

    /**
     * @Route("/addbut", name="_add-but")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    function addBut(Request $request, EntityManagerInterface $entityManager)
    {
        if ($request->isXmlHttpRequest()) {

            $matchId = $request->get('match_id');

            $rencontre = $entityManager->getRepository(Rencontre::class)->find($matchId);

            if($rencontre && !is_null($rencontre->getPremiereMiTemps()) && is_null($rencontre->getFinMatch())) {
                $rencontreUtils = new RencontreUtils();

                $but = new ButAdverse();
                $but->setTemps($rencontreUtils->getTimeSinceBeginning($rencontre))
                    ->setRencontre($rencontre);
                $rencontre->addButAdverse($but);

                $entityManager->persist($but);
                $entityManager->flush();

                return $this->json([
                    'success' => true
                ]);
            }
        }

        return $this->json([
            'success' => false
        ]);
    }

    /**
     * @Route("/add_arbitre", name="_add-arbitre")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    function addArbitre(Request $request, EntityManagerInterface $entityManager)
    {
        if ($request->isXmlHttpRequest()) {
            $matchId = $request->get('match_id');
            $arbitreInt = $request->get('arbitre');
            $principal = $request->get('principal');

          /*  dump($matchId);
            dump($arbitreInt);*/

            $rencontre = $entityManager->getRepository(Rencontre::class)->find($matchId);
            $arbitre = $entityManager->getRepository(Arbitre::class)->find($arbitreInt);

            if($rencontre) {
                $arbitreRencontre = new ArbitreRencontre();

                $arbitreRencontre->setArbitre($arbitre)
                    ->setRencontre($rencontre)
                    ->setEstPrincipal(($principal == "true" ? true : false));

                $rencontre->addArbitreMatch($arbitreRencontre);

                $entityManager->persist($arbitreRencontre);
                $entityManager->flush();

                return $this->json([
                    'success' => true
                ]);
            }
        }

        return $this->json([
            'success' => false
        ]);
    }


    /**
     * @Route("/addfaute", name="_add-faute")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    function addFaute(Request $request, EntityManagerInterface $entityManager)
    {
        if ($request->isXmlHttpRequest()) {
            $matchId = $request->get('match_id');
            $playerId = $request->get('player');
            $sanctionId = $request->get('sanction');
            $cartonId = $request->get('carton');
            $refereeId = $request->get('referee');

  /*          dump($playerId);
            dump($cartonId);
            dump($refereeId);*/

            $rencontre = $entityManager->getRepository(Rencontre::class)->find($matchId);
            $carton = $entityManager->getRepository(Carton::class)->find($cartonId);
            $sanction = $entityManager->getRepository(Sanction::class)->find($sanctionId);

            $player = $entityManager->getRepository(JoueurRencontre::class)->findOneBy([
                'id' => $playerId,
                'rencontre' => $rencontre
            ]);

            $referee = $entityManager->getRepository(ArbitreRencontre::class)->findOneBy([
                'rencontre' => $rencontre,
                'id' => $refereeId
            ]);

     /*       dump($referee);
            dump($player);*/

            if($rencontre && $player && $referee && !is_null($rencontre->getPremiereMiTemps()) && is_null($rencontre->getFinMatch())) {
                $rencontreUtils = new RencontreUtils();

                $faute = new Faute();

                $faute->setTemps($rencontreUtils->getTimeSinceBeginning($rencontre))
                    ->setCarton($carton)
                    ->setJoueur($player->getJoueur())
                    ->setSanction($sanction)
                    ->setArbitre($referee->getArbitre());

                $rencontre->addFaute($faute);

                $entityManager->persist($faute);
                $entityManager->flush();

                return $this->json([
                    'success' => true
                ]);
            }
        }

        return $this->json([
            'success' => false
        ]);
    }

    /**
     * @Route("/resumee/{id}", name="_resumee")
     * @param $id
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function resumee($id, EntityManagerInterface $entityManager) {
        $rencontre = $entityManager->getRepository(Rencontre::class)->find($id);

        $events = array_merge($rencontre->getBut()->getValues(), $rencontre->getButAdverses()->getValues(), $rencontre->getFautes()->getValues(), $rencontre->getChangements()->getValues());

        usort($events, function($arg1, $arg2) {
           if($arg1->getTemps() > $arg2->getTemps()) {
               return 1;
           } else {
               return -1;
            }
        });

        if(!$rencontre) {
            return $this->createNotFoundException('Ce match n\'existe pas');
        }



        return $this->render('rencontre/resumee.html.twig', [
            'match' => $rencontre,
            'events' => $events
        ]);
    }
}
