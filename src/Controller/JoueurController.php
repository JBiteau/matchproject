<?php

namespace App\Controller;

use App\Entity\Joueur;
use App\Form\JoueurType;
use App\Repository\JoueurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/joueurs", name="player")
 */
class JoueurController extends AbstractController
{
    /**
     * @Route("/", name="_index")
     * @param JoueurRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(JoueurRepository $repository)
    {
        $players = $repository->findAll();

        return $this->render('joueur/index.html.twig', [
            'players' => $players,
            'title' => "Liste des joueurs"
        ]);
    }

    /**
     * @Route("get-pref", name="_get-pref")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getPref(Request $request, JoueurRepository $repository) {
        if($request->isXmlHttpRequest()) {
            $id = $request->get('id');
            $player = $repository->find($id);
            dump($player);
            if($player) {
                return $this->json([
                    'success' => true,
                    'pref' => $player->getPostePredilection()->getId()
                ]);
            }
        }
        return $this->json([
            'success' => false,
        ]);
    }

    /**
     * @Route("/add", name="_add")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request, EntityManagerInterface $entityManager) {
        $player = new Joueur();
        $form = $this->createForm(JoueurType::class, $player);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $player->setEstTitulaire(true);
            $entityManager->persist($player);
            $entityManager->flush();

            $this->addFlash("success", "Le joueur a bien été ajouté");
            return $this->redirectToRoute("player_index");
        }

        return $this->render('joueur/edit.html.twig', [
           'form' => $form->createView(),
            'title' => "Ajout d'un joueur"
        ]);
    }
}
