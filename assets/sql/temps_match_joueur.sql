DROP PROCEDURE IF EXISTS temps_match_joueur;

DELIMITER $$
CREATE PROCEDURE temps_match_joueur(IN match_id INT, IN joueur_id INT)

BEGIN
	DECLARE s_entree, s_sortie, s_faute, s_change INT;
	
	SELECT COUNT(temps) INTO s_change
		FROM changement 
		WHERE (entree_joueur_id =joueur_id
		OR sortie_joueur_id = joueur_id)
		AND rencontre_id = match_id;
	
	SELECT SUM(temps) INTO s_entree
		FROM changement
		WHERE entree_joueur_id = joueur_id
		AND rencontre_id = match_id;


	SELECT SUM(temps) INTO s_sortie
		FROM changement
		WHERE sortie_joueur_id = joueur_id
		AND rencontre_id = match_id;

	SELECT (temps + temps_de_sortie) INTO s_faute
		FROM changement
		WHERE sortie_joueur_id = joueur_id
		AND temps_de_sortie <> 0
		AND rencontre_id = match_id;
		
	SELECT IF(
		(
		SELECT COUNT(jr.id) 
		FROM joueur_rencontre jr 
		WHERE (
			jr.rencontre_id = match_id AND
			jr.joueur_id = joueur_id
			) 
		)>0
	,(
	 IF(
		(s_sortie - (s_entree +IF(s_faute IS NULL,0,s_faute))) 
		 IS NULL, 90
			 ,(s_sortie - (s_entree +IF(s_faute IS NULL,0,s_faute))))),0) as temps_sur_terrain;

END $$
DELIMITER ;

CALL temps_match_joueur(1,2);
CALL temps_match_joueur(1,20);